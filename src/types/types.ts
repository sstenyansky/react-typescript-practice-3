export type ImageSlide = {
  id: string
  src: string,
  caption?: string
}
