import React from 'react'

import MainView from './view/MainView'

export default () => (
  <MainView />
)