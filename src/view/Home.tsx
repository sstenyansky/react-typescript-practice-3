import React from 'react'

import { ImageSlide } from '../types/types'

import Slider from '../component/Slider/Slider'

import { v4 as uuid } from 'uuid'

const slides: ImageSlide[] = [
  {
    id: uuid(),
    src: 'https://via.placeholder.com/500x250/FF0000/FFFFFF',
    caption: 'Caption 1'
  },
  {
    id: uuid(),
    src: 'https://via.placeholder.com/500x250/00FF00/FFFFFF',
    caption: 'Caption 2'
  },
  {
    id: uuid(),
    src: 'https://via.placeholder.com/500x250/0000FF/FFFFFF',
    caption: 'Caption 3'
  },
];

export default () => (
  <Slider
    slides={slides}
    auto
  />
)