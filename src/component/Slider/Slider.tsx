import React, { useEffect, useRef, useCallback } from 'react'

import { ImageSlide } from '../../types/types'

import useHoverDetect from '../../hook/useHoverDetect'
import useSliderController from '../../hook/useSliderController'

import Slide from '../Slide/Slide'
import NavigationControls from '../NavigationControls/NavigationControls'
import SliderPagination from '../SliderPagination/SliderPagination'

import {
  SliderStyle,
  SliderItems,
} from './Slider.styles'

type SliderProps = {
  slides: ImageSlide[]
  loop?: boolean
  navs?: boolean
  pags?: boolean
  auto?: boolean
  delay?: number
  stopMouseHover?: boolean
}

const Slider = ({
  slides,
  loop = true,
  navs = true,
  pags = true,
  auto = true,
  delay = 2000,
  stopMouseHover = true
}: SliderProps) => {
  const hover = useHoverDetect();
  const slider = useSliderController({
    loop,
    slides
  });

  const slideItems = useRef<HTMLDivElement>(null);

  const scrollTo = useCallback((pos: number) => {
    const items = slideItems.current!;

    const itemWidth = Math.floor(items.scrollWidth / slides.length);

    items.scrollLeft = pos * itemWidth;
  }, [slides.length]);

  const scrollToNext = useCallback(() => {
    const next = slider.nextSlide();

    scrollTo(next);
  }, [slider, scrollTo]);

  const scrollToPrev = useCallback(() => {
    const next = slider.prevSlide();

    scrollTo(next);
  }, [slider, scrollTo]);

  const scrollToSlide = useCallback((pos: number) => {
    slider.setSlide(pos);

    scrollTo(pos);
  }, [slider, scrollTo]);

  const nextSlideOnCd = useCallback(() => {
    if (!stopMouseHover || !hover.hovered) {
      scrollToNext();
    }
  }, [scrollToNext, hover.hovered, stopMouseHover]);

  useEffect(() => {
    let interval: NodeJS.Timer;

    if (auto) {
      interval = setInterval(nextSlideOnCd, delay);
    }

    return () => {
      if (auto) {
        clearInterval(interval);
      }
    };
  }, [nextSlideOnCd, auto, delay]);

  const sliderItems = slides.map(({
    id, src, caption
  }, i) => <Slide key={id} src={src} index={i} caption={caption} />);

  return (
    <SliderStyle onMouseEnter={hover.onMouseEnter} onMouseLeave={hover.onMouseLeave}>
      <SliderItems ref={slideItems}>
        {sliderItems}
      </SliderItems>
      {navs && <NavigationControls prevSlide={scrollToPrev} nextSlide={scrollToNext} />}
      {pags && (
        <SliderPagination
          position={slider.position}
          numSlides={slides.length}
          setSlide={scrollToSlide}
        />
      )}
    </SliderStyle>
  );
}

export default Slider