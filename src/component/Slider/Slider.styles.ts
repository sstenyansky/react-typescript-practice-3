import styled from 'styled-components'

export const SliderStyle = styled.div`
position: relative;

width: 500px;
height: 250px;
`;

export const SliderItems = styled.div`
display: flex;
flex-direction: row;

scroll-behavior: smooth;

overflow: hidden;
  
position: relative;
`;