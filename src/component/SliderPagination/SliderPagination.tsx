import React from 'react'

import classnames from 'classnames'

import { SliderPagination, SliderPaginationElement } from './SliderPagination.styles'

type SlidePaginationProps = {
  position: number
  numSlides: number
  setSlide: (index: number) => void
}

export default React.memo(({
  position,
  numSlides,
  setSlide
} : SlidePaginationProps) => {
  const paginationItems = [];

  for (let i = 0; i < numSlides; i++) {
    const classname = classnames(`slider-pagination-element-${i}`, { selected: position === i });

    paginationItems.push((
      <SliderPaginationElement
        key={i}
        className={classname}
        onClick={() => setSlide(i)}
      />
    ));
  }

  return (
    <SliderPagination>
      {paginationItems}
    </SliderPagination>
  );
})