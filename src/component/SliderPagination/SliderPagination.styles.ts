import styled from 'styled-components'

export const SliderPagination = styled.div`
display: flex;
flex-direction: row;

position: absolute;
left: 50%;
transform: translateX(-50%);
bottom: 15px;
`;

export const SliderPaginationElement = styled.div`
width: 10px;
height: 10px;

background: rgba(0,0,0, 0.4);
border-radius: 100%;

margin-right: 8px;

transition: background-color, 0.2s ease-in;

cursor: pointer;

&.selected {
  background: rgba(0,0,0, 0.2);
}

&:last-child {
  margin-right: 0;
}
`;