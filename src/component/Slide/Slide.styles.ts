import styled from 'styled-components'

export const Slide = styled.div`
position: relative;

& img {
  display: block;
} 
`;

export const SlideCaption = styled.div`
position: absolute;
left: 50%;
transform: translateX(-50%);
bottom: 40px;
`;