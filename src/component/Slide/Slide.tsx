import React from 'react'

import { Slide, SlideCaption } from './Slide.styles'

type SlideProps = {
  src: string
  caption?: string
  index: number
}

export default React.memo(({
  src,
  caption = '',
  index
}: SlideProps) => (
  <Slide className={`slider-slide-${index}`}>
    <img src={src} id={`slider-slide-${index}`} alt="" />
    {
      caption != null
      && (
        <SlideCaption>
          {caption}
        </SlideCaption>
      )
    }
  </Slide>
))