import React from 'react'

import styled from 'styled-components'

export const SliderControl = styled.button`
position: absolute;

border: none;
outline: none;
background: transparent;

cursor: pointer;

margin: 0;
padding: 0;

transform: translateY(-50%);
top: 50%;
`;

export const SliderControlNext = styled(SliderControl)`
right: 10px;
`;

export const SliderControlPrev = styled(SliderControl)`
left: 10px;
`;

export const SliderControlArrow = styled.div`
border-radius: 2px;

background: transparent;
 
width: 10px;
height: 10px;

border-color: rgba(0,0,0,0.6);
border-style: solid;

transition: border-color, 0.1s ease-in;

&:hover {
  border-color: rgba(0,0,0, 0.4);
}
`;

export const SliderControlArrowNext = React.memo(styled(SliderControlArrow)`
border-bottom-width: 4px; 
border-right-width: 4px;

border-left: none;
border-top: none;

transform: rotate(-45deg);
`);

export const SliderControlArrowPrev = React.memo(styled(SliderControlArrow)`
border-bottom-width: 4px; 
border-left-width: 4px;

border-right: none;
border-top: none;
  
transform: rotate(45deg);
`);