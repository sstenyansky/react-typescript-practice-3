import React from 'react'

import { SliderControlPrev, SliderControlNext, SliderControlArrowPrev, SliderControlArrowNext } from './NavigationControls.styles'

type NavigationControlsProps = {
  prevSlide: () => void
  nextSlide: () => void
}

export default React.memo(({
  prevSlide,
  nextSlide
} : NavigationControlsProps) => (
  <>
    <SliderControlPrev type="button" onClick={prevSlide}>
      <SliderControlArrowPrev />
    </SliderControlPrev>
    <SliderControlNext type="button" onClick={nextSlide}>
      <SliderControlArrowNext />
    </SliderControlNext>
  </>
))