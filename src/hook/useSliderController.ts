import { useState, useCallback } from 'react'

import { ImageSlide } from '../types/types'

type SliderControllerOptions = {
  initial?: number
  slides: ImageSlide[],
  loop: boolean
}

export default function useSliderController(options: SliderControllerOptions) {
  const { slides, loop, initial } = options;

  const [position, setPosition] = useState(initial ?? 0);

  const clampPosition = useCallback((pos: number) => Math.max(Math.min(pos, slides.length - 1), 0),
    [slides.length]);

  const getNextPosition = useCallback((pos: number) => {
    if (loop && pos === slides.length - 1) {
      return 0;
    }
    return clampPosition(pos + 1);
  }, [loop, slides.length, clampPosition]);

  const getPrevPosition = useCallback((pos: number) => {
    if (loop && pos === 0) {
      return slides.length - 1;
    }
    return clampPosition(pos - 1);
  }, [loop, slides.length, clampPosition]);

  const setSlide = useCallback((pos: number) => {
    setPosition(clampPosition(pos));
  }, [clampPosition]);

  const nextSlide = useCallback(() => {
    const next = getNextPosition(position);

    setPosition(next);

    return next;
  }, [position, getNextPosition]);

  const prevSlide = useCallback(() => {
    const next = getPrevPosition(position);

    setPosition(next);

    return next;
  }, [position, getPrevPosition]);

  return {
    position,
    setPosition,
    setSlide,
    prevSlide,
    nextSlide
  }
}