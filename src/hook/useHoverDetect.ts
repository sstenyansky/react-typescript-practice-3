import { useState, useCallback } from 'react'

export default function useHoverDetect(initial = false) {
  const [hovered, setHovered] = useState(initial);

  const onMouseEnter = useCallback(() => {
    setHovered(true);
  }, []);

  const onMouseLeave = useCallback(() => {
    setHovered(false);
  }, []);

  return {
    hovered,
    onMouseEnter,
    onMouseLeave
  }
}
